# This file is a template, and might need editing before it works on your project.
FROM python:3.6
EXPOSE 8002
EXPOSE 80
COPY ./app/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY * ./
#COPY . ./
#COPY . /usr/src/app
# For some other command
# CMD ["python", "app.py"]
