# Sentiment Analyzer code.

## Requiremments:

1) Python 3
2) Pip
3) Install requirements in pip file `requirements.txt`

## running the unit tests

`python tests.py`

## running the flask app

`python -m flask run`

Use HTTP POST to sent text to / to be analyzed.

## running from the command line

`python console.py "text to be analyzed"`
