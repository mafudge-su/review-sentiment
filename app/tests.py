from sentiment import analyzer
import unittest


class TestCodeModule(unittest.TestCase):

    def test_about(self):
        expect = "Sentiment Analyzer"
        self.assertTrue(analyzer.about().startswith(expect) )


    def test_sentiment(self):
        text = 'this is a painful and hurtful test'
        expect = {'text': text , 'polarity': -0.7, 'subjectivity': 0.9}
        self.assertEqual(analyzer.get_sentiment(text), expect)

    def test_pos(self):
        text = 'this is a test for you'
        expect = {'text': 'this is a test for you', 'tags': [{'words': 'this', 'pos': 'DT'}, {'word': 'is', 'pos': 'VBZ'}, {'word': 'a', 'pos': 'DT'}, {'word': 'test', 'pos': 'NN'}, {'word': 'for', 'pos': 'IN'}, {'word': 'you', 'pos': 'PRP'}]}
        self.assertEqual(analyzer.get_pos(text), expect)

if __name__ == "__main__":
    unittest.main()

