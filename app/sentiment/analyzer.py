from textblob import TextBlob 
import json 

def about():
    return "Sentiment Analyzer 10:02 AM"


def get_sentiment(text):
    blob = TextBlob(text)
    data =  { 'text': text, 
        'polarity' : blob.sentiment.polarity, 
        'subjectivity' : blob.sentiment.subjectivity }
    return data

def get_pos(text):
    blob = TextBlob(text)
    taglist = []
    for tag in blob.tags:
        taglist.append( { 'word' : tag[0], 'pos' : tag[1] })
    data = { 'text' : text, 'tags' : taglist }
    return data
