from sentiment import analyzer
from flask import Flask, request, jsonify
app = Flask(__name__)

@app.route('/', methods=['GET'])
def about():
    return analyzer.about()

@app.route('/', methods=['POST'])
def sentiment():
    text = request.form['text']
    return jsonify(analyzer.get_sentiment(text))